import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { SignInModule } from '@pages/auth/sign-in/sign-in.module';
import { SignUpModule } from '@pages/auth/sign-up/sign-up.module';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ToastrModule } from 'ngx-toastr';
import { EmployeeComponent } from './pages/employee/employee.component';
import { EmployeeEditComponent } from './components/employee-edit/employee-edit.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import { CustomerTableComponent } from './components/customer-table/customer-table.component';
import { CustomerEditComponent } from './components/customer-edit/customer-edit.component';

import { SalesComponent } from './pages/sales/sales.component';
import { LocalComponent } from './pages/local/local.component';
import { ProductComponent } from './pages/product/product.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductEditComponent } from './components/product-edit/product-edit.component';
import { LocalEditComponent } from './components/local-edit/local-edit.component';
import { LocalSaveComponent } from './components/local-save/local-save.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';
import { SaleFormComponent } from './components/sale-form/sale-form.component';
import { SaleEditComponent } from './components/sale-edit/sale-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    EmployeeComponent,
    EmployeeEditComponent,  
    CustomerFormComponent, CustomerTableComponent, CustomerEditComponent,
    EmployeeEditComponent,
    SalesComponent,
    LocalComponent,
    ProductComponent,
    ProductFormComponent,
    ProductEditComponent,  
    LocalEditComponent,
    LocalSaveComponent,
    EmployeeFormComponent,
    SaleFormComponent,
    SaleEditComponent,  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    HttpClientModule,
    SignInModule,
    SignUpModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      positionClass: 'toast-top-right',
    })
  ],
  providers: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }