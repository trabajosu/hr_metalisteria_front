import {AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClientService } from 'src/app/client.service';
import { SaleEditComponent } from 'src/app/components/sale-edit/sale-edit.component';
import { SaleFormComponent } from 'src/app/components/sale-form/sale-form.component';
import Swal from 'sweetalert2'

export interface UserData {
  id: string;
  date: string;
  iva: Number;
  methodPayment: string;
  total: Number;
  shipping: Number;
  customerDocument: Number;
}

/** Constants used to fill up our data base. */
const IVA: Number[] = [
  5,
  10,
  15,
  20
];

const PAYMENT: string[] = [
  'Contado',
  'Credito',
];

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements AfterViewInit {

  displayedColumns: string[] = ['id','date','iva', 'methodPayment', 'total', 'shipping', 'customerDocument', 'actions'];
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource = new MatTableDataSource();
  sales!: Array<any>;

  constructor(private clientService: ClientService, private dialog: MatDialog, private readonly router: Router, private readonly toastrSvc: ToastrService) { 
    this.consultSales();
  }
  
  consultSales() {
    this.clientService.getRequestConsultToken('http://localhost:10101/sale/consult').subscribe(
      (response: any) => {
        this.sales = response.sales;
        this.dataSource.data = this.sales;
        console.log(response.sales);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogFrom(): void {
    const config = {
      data: {
        message:  'Agregar Cliente'
      }
    };
    const dialogRef = this.dialog.open(SaleFormComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultSales();
    });
  }

  onEditSale(element: any): void {

    const config = {
      data: {
        message: element ? 'Editar Cliente' : 'Error',
        content: element
      }
    };
    const dialogRef = this.dialog.open(SaleEditComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultSales();
    });
  }
  
  onDeleteAale(element: any) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar con seguridad?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Eliminar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      //Read more about isConfirmed, isDenied below
      if (result.isConfirmed) {
        this.clientService.deleteRequestDelete(`http://localhost:10101/sale/delete/?id=${element.id}`).subscribe(
          (response: any) => {
            
            this.consultSales();
          },
          (error) => {
            if (error.error.auth == false) {
              this.expiredToken();
            }
            console.log(error.status);
          }
        );
      } else if (result.isDenied) {
        Swal.fire('El cliente no se ha eliminado', '', 'info')
      }
    });
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}