import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@pages/auth/guards/auth.guard';
import { DashboardComponent } from '@pages/dashboard/dashboard.component';
import { EmployeeComponent } from '@pages/employee/employee.component';
import { LocalComponent } from '@pages/local/local.component';
import { SalesComponent } from '@pages/sales/sales.component';
import { NavigationComponent } from '../components/navigation/navigation.component';
import { ProductComponent } from './product/product.component';
import { CustomerTableComponent } from '../components/customer-table/customer-table.component';
import { RolesGuard } from './auth/guards/roles.guard';

const routes: Routes = [
  { path: '', component: NavigationComponent, canActivate: [AuthGuard],
    children: [
      { path: 'reports', component: DashboardComponent, data: { role: ['1', '2'] }, canActivate: [AuthGuard, RolesGuard] },
      { path: 'employee', component: EmployeeComponent, data: { role: ['1', '2', '5'] }, canActivate: [AuthGuard, RolesGuard] },
      { path: 'customer', component: CustomerTableComponent, data: { role: ['1', '4'] }, canActivate: [AuthGuard, RolesGuard] },
      { path: 'sales', component: SalesComponent, data: { role: ['1', '4'] }, canActivate: [AuthGuard, RolesGuard] },
      { path: 'local', component: LocalComponent, data: { role: ['1'] }, canActivate: [AuthGuard, RolesGuard] },
      { path: 'product', component: ProductComponent, data: { role: ['1', '3', '4'] }, canActivate: [AuthGuard, RolesGuard] },
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ] },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }