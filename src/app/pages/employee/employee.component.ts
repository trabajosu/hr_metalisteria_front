import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA, ViewChild } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2'
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClientService } from '../../client.service';
import { EmployeeEditComponent } from 'src/app/components/employee-edit/employee-edit.component';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeFormComponent } from 'src/app/components/employee-form/employee-form.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  currentRole: string = '' + localStorage.getItem("role");
  employees!: Array<any>;
  displayedColumns: string[] = ['id', 'name', 'phoneNumber', 'address', 'email', 'localName', 'roleName', 'cityName', 'actions'];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource = new MatTableDataSource();

  constructor(private client: ClientService, private dialog: MatDialog, private router: Router, private toastrSvc: ToastrService) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit(): void {
    this.consultEmployee();
  }

  consultEmployee() {
    this.client.getRequestConsultToken('http://localhost:10101/user/consultAll').subscribe(
      (response: any) => {
        this.employees = response.users;
        this.dataSource.data = this.employees;
        console.log(response.users);
      },
      (error) => {
        console.log(error);
        
        if (error.error.auth == false){
          this.expiredToken();
        }
      }
    )
  }

  openDialogFormEmployee() {
    const dialogRef = this.dialog.open(EmployeeFormComponent, {
      width: '600px',
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {
      this.consultEmployee();
    });
  }

  openDialog(element: any): void {

    const config = {
      data: {
        message: element ? 'Editar Cliente' : 'Error',
        content: element
      }
    };
    const dialogRef = this.dialog.open(EmployeeEditComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultEmployee();
    });
  }

  onDeleteEmployee(element: any) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar con seguridad?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Eliminar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      //Read more about isConfirmed, isDenied below
      if (result.isConfirmed) {
        this.client.deleteRequestDelete(`http://localhost:10101/user/adminDelete/?id=${element.id}`).subscribe(
          (response: any) => {
            
            for (const key in this.employees) {
              if (this.employees[key].document === element.document) {
                this.employees.splice(parseInt(key), 1);
                break;
              }
            }
            this.consultEmployee();

          },
          (error) => {
            if (error.error.auth == false) {
              this.expiredToken();
            }
            console.log(error.status);
          }
        );
      } else if (result.isDenied) {
        Swal.fire('El empleado no se ha eliminado', '', 'info')
      }
    });
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
