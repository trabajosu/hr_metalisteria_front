import {AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClientService } from 'src/app/client.service';
import { LocalEditComponent } from 'src/app/components/local-edit/local-edit.component';
import { LocalSaveComponent } from 'src/app/components/local-save/local-save.component';
import Swal from 'sweetalert2'

export interface localData {
  id: number;
  name: string;
  address: string;
  nombre: string;
}

@Component({
  selector: 'app-local',
  templateUrl: './local.component.html',
  styleUrls: ['./local.component.css']
})
export class LocalComponent implements  AfterViewInit  {

  displayedColumns: string[] = ['id', 'name', 'address', 'cityName', 'actions'];
  // dataSource: MatTableDataSource<localData>;
  locals!: Array<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  dataSource = new MatTableDataSource();

  constructor(private clientService: ClientService, private dialog: MatDialog, private readonly router: Router, private readonly toastrSvc: ToastrService) { 
    this.consultLocal();
  }

  consultLocal() {
    this.clientService.getRequestConsult('http://localhost:10101/local/consult').subscribe(
      (response: any) => {
        this.locals = response.locals;
        this.dataSource.data = this.locals;
        console.log(response.locals);
      },
      (error) => {
        console.log(error);
      }
    )
  }
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(element:any): void {

    const config = {
      data: {
        message:  'Editar Local',
        content: element
      }
    };
    const dialogRef = this.dialog.open(LocalEditComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultLocal();
    });
  }

  openDialogFrom(): void {
    const config = {
      data: {
        message:  'Agregar Cliente'
      }
    };
    const dialogRef = this.dialog.open(LocalSaveComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultLocal();
    });
  }

  deleteLocal(element:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Estas Seguro De Eliminar?',
      text: "No podrás revertir esto.!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'si, Eliminar!',
      cancelButtonText: 'No, Cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.clientService.deleteRequest(`http://localhost:10101/local/delete/?id=${element.id}`).subscribe(
          (response: any) => {
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'El Local fue eliminado.',
              'success'
            );
            this.consultLocal();
          },
          //si ocurre un error en el proceso de envío del formulario...
          (error) => {
            if (error.error.auth == false) {
              localStorage.clear();
              this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
              this.router.navigate(['/sign-in']);
            }else{
              Swal.fire({
                icon: 'warning',
                title: 'Problemas en el servidor',
                text: 'En estos momentos tenemos fallas en el sistema ' + error.status,
                background: '#fff',
                confirmButtonColor: '#045b62'
              });
            }
            //se imprime el status del error
            console.log(error.status);
          })
        
        
      } else if (
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Se cancelo la eliminacion',
          'error'
        )
      }
    })
  }
}
