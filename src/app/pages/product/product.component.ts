import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2'
import { MatTableDataSource } from '@angular/material/table';
import { CustomerEditComponent } from '../../components/customer-edit/customer-edit.component';
import { ProductFormComponent } from 'src/app/components/product-form/product-form.component';
import { ProductEditComponent } from 'src/app/components/product-edit/product-edit.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, AfterViewInit {
  currentRole: string = '' + localStorage.getItem("role");
  products!: Array<any>;
  displayedColumns: string[] = ['code', 'name', 'description', 'iva', 'price', 'actions'];
  dataSource = new MatTableDataSource();


  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  constructor(private clientService: ClientService, private dialog: MatDialog, private readonly router: Router, private readonly toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.consultProducts();
  }

  consultProducts() {
    this.clientService.getRequestConsultToken('http://localhost:10101/product/consult').subscribe(
      (response: any) => {
        this.products = response.product;
        this.dataSource.data = this.products;
        console.log(response);
      },
      (error) => {
        if (error.error.auth == false) {
          this.expiredToken();
        }
        console.log(error);
      }
    )
  }

  onEditProduct(element: any) {
    this.openDialog(element);
  }
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(element: any): void {
    const config = {
      data: {
        message: element ? 'Editar Cliente' : 'Error',
        content: element
      }
    };
    const dialogRef = this.dialog.open(ProductEditComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultProducts();
    });
  }

  onDeleteProduct(element: any) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar con seguridad?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Eliminar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      //Read more about isConfirmed, isDenied below
      if (result.isConfirmed) {
        this.clientService.deleteRequestDelete(`http://localhost:10101/product/delete/?code=${element.code}`).subscribe(
          (response: any) => {
            console.log(response);
            for (const key in this.products) {
              if (this.products[key].code === element.code) {
                this.products.splice(parseInt(key), 1);
                break;
              }
            }
            this.consultProducts();
          },
          (error) => {
            if (error.error.auth == false) {
              this.expiredToken();
            }
            console.log(error.status);
          }
        );
      } else if (result.isDenied) {
        Swal.fire('El producto no se ha eliminado', '', 'info')
      }
    });
  }

  openDialogFormProduct(): void {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      width: '500px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this. consultProducts();
    });
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }

}
