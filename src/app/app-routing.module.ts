import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@pages/auth/guards/auth.guard';
import { AuthLoginGuard } from '@pages/auth/guards/authLogin.guard';

const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/auth/sign-in/sign-in.module').then(m => m.SignInModule), canActivate: [AuthLoginGuard] },
  { path: 'navigation',loadChildren: ()=> import('./pages/page-routing.module').then((m) => m.PageRoutingModule), canActivate: [AuthGuard]},
  { path: 'sign-in', loadChildren: () => import('./pages/auth/sign-in/sign-in.module').then(m => m.SignInModule), canActivate: [AuthLoginGuard] },
  { path: 'sign-up', loadChildren: () => import('./pages/auth/sign-up/sign-up.module').then(m => m.SignUpModule), canActivate: [AuthLoginGuard]},
  { path: '**', redirectTo: '', pathMatch: 'full' },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }