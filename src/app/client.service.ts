import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  [x: string]: any;
  
  constructor(private http: HttpClient) { }

  deleteRequestDelete(route: string) {
    let config: any = {
      responseType: "json"
    }

    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.delete(route, config);
  }

  getRequestConsult(route: string) {
    let config: any = {
      responseType: "json"
    }

    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    //Notese que como tercer parametro se pasa la configuracion de la request
    return this.http.get(route, config);
  }

  postRequestSendForm(route: string, data: any) {
    let config: any = {
      responseType: 'json',
    }

    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.post(route, data, config);

  }
  getRequestConsultToken(route: string) {
    let config: any = {
      responseType: "json"
    }

    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);    
    config["headers"] = header;

    //Notese que como tercer parametro se pasa la configuracion de la request
    return this.http.get(route, config);
  }

  putRequestUpdate(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }

    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.put(route, data, config);
  }

  deleteRequest(route: string) {
    let config: any = {
      responseType: "json"
    }

    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.delete(route, config);
  }
}
