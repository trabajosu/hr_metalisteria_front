import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../../client.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {

  spinner: boolean = false;
  form!: FormGroup;
  cities!: Array<any>;
  roles!: Array<any>;
  locals!: Array<any>;

  constructor(private client: ClientService, private formBuilder: FormBuilder, private router: Router,
    public dialog: MatDialogRef<EmployeeFormComponent>, private readonly toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.consultCities();
    this.consultLocal();
    this.consultRole();
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      localCode: ['', Validators.required],
      roleCode: ['', Validators.required],
      cityCode: ['', Validators.required],
    });
  }

  consultCities() {
    this.client.getRequestConsult("http://localhost:10101/city/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.cities = response.cities;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  consultLocal() {
    this.client.getRequestConsult("http://localhost:10101/local/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.locals = response.locals;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  consultRole() {
    this.client.getRequestConsult("http://localhost:10101/role/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.roles = response.roles;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinner = true;

      this.client.postRequestSendForm('http://localhost:10101/user/adminRegister', {
        name: this.form.value.name,
        phoneNumber: this.form.value.phoneNumber,
        address: this.form.value.address,
        email: this.form.value.email,
        password: this.form.value.password,
        localCode: this.form.value.localCode,
        roleCode: this.form.value.roleCode,
        cityCode: this.form.value.cityCode,
        
      }).subscribe(
        (response: any) => {
          this.dialog.close(true);

          this.spinner = false;
          Swal.fire({
            icon: 'success',
            title: 'Registro exitoso',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
        }, (error) => {
          this.spinner = false;
          if (error.error.auth == false) {
            this.expiredToken();
          }
          console.log(error.status);
        }
      );
    }
  }
  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
