import { Component, OnInit, AfterViewInit, ViewChild, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2'
import { MatTableDataSource } from '@angular/material/table';
import { CustomerEditComponent } from '../../components/customer-edit/customer-edit.component';
import { CustomerFormComponent } from '../customer-form/customer-form.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.css']
})
export class CustomerTableComponent implements OnInit, AfterViewInit {
  currentRole: string = '' + localStorage.getItem("role");
  customers!: Array<any>;
  displayedColumns: string[] = ['document', 'name', 'email', 'phoneNumber', 'address', 'cityName', 'actions'];
  dataSource = new MatTableDataSource();


  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  constructor(private clientService: ClientService, private dialog: MatDialog, private readonly router: Router, private readonly toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.consultCustomers();
  }

  consultCustomers() {
    this.clientService.getRequestConsultToken('http://localhost:10101/customer/consult').subscribe(
      (response: any) => {
        this.customers = response.customer;
        this.dataSource.data = this.customers;
        console.log(response.customer);
      },
      (error) => {
        if (error.error.auth == false) {
          this.expiredToken();
        }
        console.log(error);
      }
    )
  }

  onEditCustomer(element: any) {
    this.openDialog(element);
  }
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(element: any): void {

    const config = {
      data: {
        message: element ? 'Editar Cliente' : 'Error',
        content: element
      }
    };
    const dialogRef = this.dialog.open(CustomerEditComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      this.consultCustomers();
    });
  }

  onDeleteCustomer(element: any) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar con seguridad?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Eliminar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      //Read more about isConfirmed, isDenied below
      if (result.isConfirmed) {
        this.clientService.deleteRequestDelete(`http://localhost:10101/customer/delete/?document=${element.document}`).subscribe(
          (response: any) => {
            
            for (const key in this.customers) {
              if (this.customers[key].document === element.document) {
                this.customers.splice(parseInt(key), 1);
                break;
              }
            }
            this.consultCustomers();
          },
          (error) => {
            if (error.error.auth == false) {
              this.expiredToken();
            }
            console.log(error.status);
          }
        );
      } else if (result.isDenied) {
        Swal.fire('El cliente no se ha eliminado', '', 'info')
      }
    });
  }

  openDialogFormCustomer(): void {
    const dialogRef = this.dialog.open(CustomerFormComponent, {
      width: '600px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this. consultCustomers();
    });
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
