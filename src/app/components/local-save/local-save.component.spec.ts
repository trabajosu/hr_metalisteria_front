import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalSaveComponent } from './local-save.component';

describe('LocalSaveComponent', () => {
  let component: LocalSaveComponent;
  let fixture: ComponentFixture<LocalSaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocalSaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
