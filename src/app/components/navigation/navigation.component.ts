import { Component, Input } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '@pages/auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  paths: any = {
    reports: '/navigation/reports',
    employee: '/navigation/employee',
    customer: '/navigation/customer',
    sales: '/navigation/sales',
    local: '/navigation/local',
    product: '/navigation/product'
  };
  currentRole:string= ''+localStorage.getItem("role");
  userName: string | null = localStorage.getItem("name");
  currentPath!: string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private readonly authSvc: AuthService, private router: Router) {
    this.currentPath = router.url;
  }

  goToPage(path: string) {
    this.currentPath = path;
    this.router.navigate([path]);    
  }

  showUpdateData() {

  }

  onLogout() {
    localStorage.clear();
  }
}
