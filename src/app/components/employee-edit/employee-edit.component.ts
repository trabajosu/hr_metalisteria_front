import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientService } from 'src/app/client.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent {
  name = new FormControl('', [Validators.required, Validators.minLength(3)]);
  weight = new FormControl('', [Validators.required, Validators.min(10)]);

  constructor(private client: ClientService, private fb: FormBuilder, public dialog: MatDialogRef<EmployeeEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private readonly router: Router, private readonly toastrSvc: ToastrService) { }

  spinner: boolean = false;
  element = this.data.content;
  form!: FormGroup;
  cities!: Array<any>;
  locals!: Array<any>;
  roles!: Array<any>;

  ngOnInit(): void {
    this.consultCities();
    this.consultLocal();
    this.consultRole();
    this.form = this.fb.group({
      name: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      localCode: ['', Validators.required],
      roleCode: ['', Validators.required],
      cityCode: ['', Validators.required],
    });
    this.initValuesForm();
  }
  consultCities() {
    this.client.getRequestConsult("http://localhost:10101/city/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.cities = response.cities;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  consultLocal() {
    this.client.getRequestConsult("http://localhost:10101/local/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.locals = response.locals;
        console.log(this.locals);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  consultRole() {
    this.client.getRequestConsult("http://localhost:10101/role/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.roles = response.roles;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }
  
  private initValuesForm(): void {
    this.form.patchValue({
      name: this.element.name,
      phoneNumber: this.element.phoneNumber,
      address: this.element.address,
      email: this.element.email,
      localCode: this.element.localCode,
      roleCode: this.element.roleCode,
      cityCode: this.element.cityCode,
    });
  }

  onSubmit() {
    Swal.fire({
      icon: 'question',
      title: '¿Desea guardar los cambios?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Guardar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      if (result.isConfirmed) {
        //si la validacion del formulario es exitosa...
        if (this.form.valid) {
          this.spinner = true;
          //se envian los datos del formulario mediante una solicitud POST, los valores de los inputs del formulario 
          //se recogen usando los controles "email" y "password" para formar el json a enviar..
          this.client.putRequestUpdate(`http://localhost:10101/user/adminUpdate/?id=${this.element.id}`, {
            name: this.form.value.name,
            phoneNumber: this.form.value.phoneNumber,
            address: this.form.value.address,
            email: this.form.value.email,
            localCode: this.form.value.localCode,
            roleCode: this.form.value.roleCode,
            cityCode: this.form.value.cityCode,
          }).subscribe(
            //cuando la respuesta del server llega es emitida por el observable mediante next()..
            (response: any) => {
              this.spinner = false;

              this.dialog.close(true);
              Swal.fire({
                icon: 'success',
                title: 'Empleado modificado con exito',
                background: '#fff',
                confirmButtonColor: '#045b62'
              });
            },
            //si ocurre un error en el proceso de envío del formulario...
            (error) => {
              this.spinner = false;
              if (error.error.auth == false) {
                this.expiredToken();
              }{
                Swal.fire({
                  icon: 'warning',
                  title: 'Problemas en el servidor',
                  text: 'En estos momentos tenemos fallas en el sistema ' + error.status,
                  background: '#fff',
                  confirmButtonColor: '#045b62'
                });
              }
              //se imprime el status del error
              console.log(error.status);
            })
          //si ocurrio un error en la validacion del formulario este no se enviara
          //y se imprimira el mensaje "Form error"
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Datos invalidos',
            text: 'Por favor diligencie los datos correctamente',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
          console.log("Form error");
        }
      } else if (result.isDenied) {
        Swal.fire('Los cambios no han sido guardados', '', 'info')
      }
    })
  }
  cancelModify() {
    this.dialog.close(true);
  }
  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
