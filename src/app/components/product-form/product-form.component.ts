import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../../client.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  spinner: boolean = false;
  form!: FormGroup;

  constructor(private clienteService: ClientService, private formBuilder: FormBuilder, private router: Router,
    public dialog: MatDialogRef<ProductFormComponent>, private toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      code: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(5)]],
      name: ['', Validators.required],
      description: ['', Validators.required],
      iva: ['', Validators.required],
      price: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinner = true;

      this.clienteService.postRequestSendForm('http://localhost:10101/product/register', {
        code: this.form.value.code,
        name: this.form.value.name,
        description: this.form.value.description,
        iva: this.form.value.iva,
        price: this.form.value.price,        
      }).subscribe(
        (response: any) => {
          this.dialog.close(true);

          this.spinner = false;
          Swal.fire({
            icon: 'success',
            title: 'Registro exitoso',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
        }, (error: any) => {
          this.spinner = false;
          if (error.error.auth == false) {
            this.expiredToken();
          }
          console.log(error.status);
        }
      )
    }
  }
  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
