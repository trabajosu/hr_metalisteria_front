import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClientService } from 'src/app/client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sale-form',
  templateUrl: './sale-form.component.html',
  styleUrls: ['./sale-form.component.css']
})
export class SaleFormComponent implements OnInit {

  form!: FormGroup;
  users!: Array<any>;
  locals!: Array<any>;
  spinner: boolean = false;

  constructor(private clienteService: ClientService, private formBuilder: FormBuilder, private router: Router,
    public dialog: MatDialogRef<SaleFormComponent>, private toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.consultCities();
    this.consultLocal();
    this.form = this.formBuilder.group({
      date: [''],
      iva: ['', Validators.required],
      methodPayment: ['', Validators.required],
      shipping: ['', [Validators.required]],
      total: ['', [Validators.required]],
      localCode: ['', Validators.required],
      customerDocument: ['', Validators.required]
    });
  }

  validateShipping(event: any) {
    if (this.form.value.total > 200000){
      this.form.controls["shipping"].setValue(0);
      this.form.controls["shipping"].disable();
    }else{
      this.form.controls["shipping"].enable();
    }
  }

  consultCities() {
    this.clienteService.getRequestConsult("http://localhost:10101/customer/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.users = response.customer;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  consultLocal() {
    this.clienteService.getRequestConsult("http://localhost:10101/local/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.locals = response.locals;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinner = true;
      this.clienteService.postRequestSendForm('http://localhost:10101/sale/register', {
        date: this.form.value.date,
        iva: this.form.value.iva,
        methodPayment: this.form.value.methodPayment,
        total: this.form.value.total,
        shipping: this.form.value.shipping || 0,
        localCode: this.form.value.localCode,
        customerDocument: this.form.value.customerDocument,
      }).subscribe(
        (response: any) => {
          this.dialog.close(true);

          this.spinner = false;
          Swal.fire({
            icon: 'success',
            title: 'Registro exitoso',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
        }, (error) => {
          this.spinner = false;
          if (error.error.auth == false) {
            this.expiredToken();
          }
          console.log(error.status);
        }
      );
    }
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
