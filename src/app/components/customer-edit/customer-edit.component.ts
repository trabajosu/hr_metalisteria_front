import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {

  constructor(private client: ClientService, private fb: FormBuilder, public dialog: MatDialogRef<CustomerEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private readonly router: Router, private readonly toastrSvc: ToastrService) {  }

  spinner: boolean = false;
  element = this.data.content;
  form!: FormGroup;
  cities!: Array<any>;

  ngOnInit(): void {
    this.consultCities();
    this.form = this.fb.group({
      document: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]],
      name: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      cityCode: ['', Validators.required],
    });
    this.initValuesForm();
  }
  consultCities() {
    this.client.getRequestConsult("http://localhost:10101/city/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.cities = response.cities;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  private initValuesForm(): void {    
    this.form.patchValue({
      document: this.element.document,
      name: this.element.name,
      email: this.element.email,
      phoneNumber: this.element.phoneNumber,
      address: this.element.address,
      cityCode: this.element.cityCode,
    });
  }

  cancelModify() {
    this.dialog.close(true);
  }

  onSubmit() {
    Swal.fire({
      icon: 'question',
      title: '¿Desea guardar los cambios?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Guardar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      if (result.isConfirmed) {
        //si la validacion del formulario es exitosa...
        if (this.form.valid) {
          this.spinner = true;
          //se envian los datos del formulario mediante una solicitud POST, los valores de los inputs del formulario 
          //se recogen usando los controles "email" y "password" para formar el json a enviar..
          this.client.putRequestUpdate(`http://localhost:10101/customer/update/?document=${this.element.document}`, {
            name: this.form.value.name,
            email: this.form.value.email,
            phoneNumber: this.form.value.phoneNumber,
            address: this.form.value.address,
            cityCode: this.form.value.cityCode,
          }).subscribe(
            //cuando la respuesta del server llega es emitida por el observable mediante next()..
            (response: any) => {
              this.spinner = false;
              //se imprime la respuesta del server
              this.element.name = this.form.value.name;
              this.element.email = this.form.value.email;
              this.element.address = this.form.value.address;
              this.element.phoneNumber = this.form.value.phoneNumber;
              this.element.cityCode = this.form.value.cityCode;
              for (const iterator of this.cities) {
                if (iterator.code === this.element.cityCode) {
                  this.element.cityName = iterator.name;
                  break;
                }
              }
              this.dialog.close(true);
              Swal.fire({
                icon: 'success',
                title: 'Cliente modificado con exito',
                background: '#fff',
                confirmButtonColor: '#045b62'
              });
            },
            //si ocurre un error en el proceso de envío del formulario...
            (error) => {
              this.spinner = false;
              if (error.error.auth == false) {
                this.expiredToken();
              }else{
                Swal.fire({
                  icon: 'warning',
                  title: 'Problemas en el servidor',
                  text: 'En estos momentos tenemos fallas en el sistema ' + error.status,
                  background: '#fff',
                  confirmButtonColor: '#045b62'
                });
              }
              //se imprime el status del error
              console.log(error.status);
            })
          //si ocurrio un error en la validacion del formulario este no se enviara
          //y se imprimira el mensaje "Form error"
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Datos invalidos',
            text: 'Por favor diligencie los datos correctamente',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
          console.log("Form error");
        }
      } else if (result.isDenied) {
        Swal.fire('Los cambios no han sido guardados', '', 'info')
      }
    })
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }

}
