import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  constructor(private client: ClientService, private fb: FormBuilder, public dialog: MatDialogRef<ProductEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private router: Router, private toastrSvc: ToastrService) {  }

  spinner: boolean = false;
  element = this.data.content;
  form!: FormGroup;
  cities!: Array<any>;

  ngOnInit(): void {
    this.form = this.fb.group({
      code: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(5)]],
      name: ['', Validators.required],
      description: ['', Validators.required],
      iva: ['', Validators.required],
      price: ['', Validators.required],
    });
    this.initValuesForm();
  }

  private initValuesForm(): void {    
    this.form.patchValue({
      code: this.element.code,
      name: this.element.name,
      email: this.element.email,
      description: this.element.description,
      iva: this.element.iva,
      price: this.element.price,
    });
  }

  cancelModify() {
    this.dialog.close(true);
  }

  onSubmit() {
    Swal.fire({
      icon: 'question',
      title: '¿Desea guardar los cambios?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Guardar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      if (result.isConfirmed) {
        //si la validacion del formulario es exitosa...
        if (this.form.valid) {
          this.spinner = true;
          //se envian los datos del formulario mediante una solicitud POST, los valores de los inputs del formulario 
          //se recogen usando los controles "email" y "password" para formar el json a enviar..
          this.client.putRequestUpdate(`http://localhost:10101/product/update/?code=${this.element.code}`, {
            code: this.form.value.code,
            name: this.form.value.name,
            description: this.form.value.description,
            iva: this.form.value.iva,
            price: this.form.value.price,
          }).subscribe(
            //cuando la respuesta del server llega es emitida por el observable mediante next()..
            (response: any) => {
              this.spinner = false;
              //se imprime la respuesta del server
              console.log('hola', response);
              //this.element.code = this.form.value.name;
              this.element.name = this.form.value.name;
              this.element.description = this.form.value.description;
              this.element.iva = this.form.value.iva;
              this.element.price = this.form.value.price;

              this.dialog.close(true);
              Swal.fire({
                icon: 'success',
                title: 'Producto modificado con exito',
                background: '#fff',
                confirmButtonColor: '#045b62'
              });
            },
            //si ocurre un error en el proceso de envío del formulario...
            (error) => {
              this.spinner = false;
              if (error.error.auth == false) {
                this.expiredToken();
              }else {
                Swal.fire({
                  icon: 'warning',
                  title: 'Problemas en el servidor',
                  text: 'En estos momentos tenemos fallas en el sistema ' + error.status,
                  background: '#fff',
                  confirmButtonColor: '#045b62'
                });
              }
              //se imprime el status del error
              console.log(error.status);
            })
          //si ocurrio un error en la validacion del formulario este no se enviara
          //y se imprimira el mensaje "Form error"
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Datos invalidos',
            text: 'Por favor diligencie los datos correctamente',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
          console.log("Form error");
        }
      } else if (result.isDenied) {
        Swal.fire('Los cambios no han sido guardados', '', 'info')
      }
    })
  }
  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
