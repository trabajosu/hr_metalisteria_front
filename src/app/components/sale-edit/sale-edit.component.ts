import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClientService } from 'src/app/client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sale-edit',
  templateUrl: './sale-edit.component.html',
  styleUrls: ['./sale-edit.component.css']
})
export class SaleEditComponent implements OnInit {
  form!: FormGroup;
  users!: Array<any>;
  locals!: Array<any>;
  spinner: boolean = false;
  element = this.data.content;

  constructor(private clienteService: ClientService, private fb: FormBuilder, public dialog: MatDialogRef<SaleEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private readonly router: Router, private readonly toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.consultCities();
    this.consultLocal();
    this.form = this.fb.group({
      date: ['', ],
      iva: ['', Validators.required],
      methodPayment: ['', Validators.required],
      total: ['', [Validators.required]],
      shipping: ['', [Validators.required]],
      localCode: ['', Validators.required],
      customerDocument: ['', Validators.required]
    });
      this.initValuesForm();
  }

  private initValuesForm(): void {
    this.form.patchValue({
      date: this.element.date,
      iva: this.element.iva,
      methodPayment: this.element.methodPayment,
      total: this.element.total,
      shipping: this.element.shipping,
      localCode: this.element.localCode,
      customerDocument: this.element.customerDocument,
    });
  }

  onSubmit() {
    Swal.fire({
      icon: 'question',
      title: '¿Desea guardar los cambios?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Guardar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      if (result.isConfirmed) {
        //si la validacion del formulario es exitosa...
        if (this.form.valid) {
          this.spinner = true;
          this.clienteService.putRequestUpdate(`http://localhost:10101/sale/update/?id=${this.element.id}`, {
            iva: this.form.value.iva,
            methodPayment: this.form.value.methodPayment,
            total: this.form.value.total,
            shipping: this.form.value.shipping,
            localCode: this.form.value.localCode,
            customerDocument: this.form.value.customerDocument,
          }).subscribe(
            (response: any) => {
              this.spinner = false;

              this.dialog.close(true);
              Swal.fire({
                icon: 'success',
                title: 'Venta modificada con exito',
                background: '#fff',
                confirmButtonColor: '#045b62'
              });
            },
            //si ocurre un error en el proceso de envío del formulario...
            (error) => {
              this.spinner = false;
              if (error.error.auth == false) {
                this.expiredToken();
              }{
                Swal.fire({
                  icon: 'warning',
                  title: 'Problemas en el servidor',
                  text: 'En estos momentos tenemos fallas en el sistema ' + error.status,
                  background: '#fff',
                  confirmButtonColor: '#045b62'
                });
              }
              //se imprime el status del error
              console.log(error.status);
            })
          //si ocurrio un error en la validacion del formulario este no se enviara
          //y se imprimira el mensaje "Form error"
        } else {
          Swal.fire({
            icon: 'warning',
            title: 'Datos invalidos',
            text: 'Por favor diligencie los datos correctamente',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
          console.log("Form error");
        }
      } else if (result.isDenied) {
        Swal.fire('Los cambios no han sido guardados', '', 'info')
      }
    })
  }
  consultCities() {
    this.clienteService.getRequestConsult("http://localhost:10101/customer/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.users = response.customer;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  consultLocal() {
    this.clienteService.getRequestConsult("http://localhost:10101/local/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.locals = response.locals;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        console.log(error.status);
      }
    )
  }

  cancelModify() {
    this.dialog.close(true);
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}
