import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../../client.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  spinner: boolean = false;
  form!: FormGroup;
  cities!: Array<any>;

  constructor(private clienteService: ClientService, private formBuilder: FormBuilder, private router: Router,
    public dialog: MatDialogRef<CustomerFormComponent>, private readonly toastrSvc: ToastrService) { }

  ngOnInit(): void {
    this.consultCities();
    this.form = this.formBuilder.group({
      document: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.required],
      address: ['', Validators.required],
      cityCode: ['', Validators.required],
    });
  }

  consultCities(){
    this.clienteService.getRequestConsult("http://localhost:10101/city/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.cities = response.cities;
        console.log(response);
    },
    //si ocurre un error en el proceso de envío del formulario...
    (error) => {
      console.log(error.status);
      }
    )
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinner = true;

      this.clienteService.postRequestSendForm('http://localhost:10101/customer/register', {
        document: this.form.value.document,
        name: this.form.value.name,
        email: this.form.value.email,
        phoneNumber: this.form.value.phoneNumber,
        address: this.form.value.address,
        cityCode: this.form.value.cityCode,
        
      }).subscribe(
        (response: any) => {
          this.dialog.close(true);

          this.spinner = false;
          Swal.fire({
            icon: 'success',
            title: 'Registro exitoso',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
        }, (err: any) => {
          this.spinner = false;
          if (err.error.auth == false) {
            this.expiredToken();
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: err.error.message,
              background: '#fff',
              confirmButtonColor: '#045b62'
            });
          }
        }
      );
    }
  }

  expiredToken() {
    localStorage.clear();
    this.toastrSvc.warning('Su sesión ha expirado', 'Advertencia');
    this.router.navigate(['/sign-in']);
  }
}